## RUN APP

```bash
./run
```

hoặc

```bash
java -jar Release/VietAnhDict_all.jar
```

## BUILD APP

* IDE sử dụng **IntelliJ IDEA Community**

* Build bằng **Gradle**

* Các bước để build:

```bash
# Linux
./gradlew release

# Windows
./gradlew.bat release
```
