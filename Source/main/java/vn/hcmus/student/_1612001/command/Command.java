package vn.hcmus.student._1612001.command;

public enum Command {
  help,
  t,
  add,
  rm,
  stat,
  sw,
  quit,
  exit,
}
