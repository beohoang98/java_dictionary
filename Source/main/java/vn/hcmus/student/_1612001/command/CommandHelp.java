package vn.hcmus.student._1612001.command;

import java.util.Map;

public class CommandHelp {
  public static Map<Command, String> descrips = Map.of(
      Command.help, "Hiển thị tất cả câu lệnh",
      Command.exit, "Thoát app",
      Command.quit, "Thoát app - giống 'exit'",
      Command.add, "add <word> <meaning> - Thêm từ <word> cùng với nghĩa <meaning>" +
          "\nadd <word> - Thêm từ <word>, sau đó nhập nghĩa, kết thúc bằng Ctrl + Enter",
      Command.rm, "rm <word> - Xóa từ <word> ra khỏi từ điển",
      Command.stat, "stat - In thống kê keyword sắp xếp theo số lần" +
          "\nstat <dd-MM-yyyy> - In thống kê bắt đầu từ ngày dd-MM-yyyy" +
          "\nstat <dd-MM-yyyy> <dd-MM-yyyy> - In thống kê từ ngày này đền ngày kia",
      Command.sw, "sw <lang = en|vi> - Đổi ngôn ngữ qua lại giữa <en> và <vi>",
      Command.t, "t <word> - Dịch từ <word>"
  );

  public static String helpOf(Command command) {
    return descrips.get(command);
  }
}
