package vn.hcmus.student._1612001.command;

import com.github.tomaslanger.chalk.Chalk;
import com.sun.istack.NotNull;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import vn.hcmus.student._1612001.services.LogService;

public class Prompt {
  private String prefix = ">> ";
  private Scanner scanner = new Scanner(System.in);
  private final Map<String, CommandHandler> events = new HashMap<>();

  public Prompt() {}

  public void setScannerIn(InputStream inputStream) {
    this.scanner = new Scanner(inputStream);
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public void run() {
    while (true) {
      System.out.print(Chalk.on(prefix).bold());
      String inp = scanner.nextLine();

      ArrayList<String> args = new ArrayList<>(Arrays.asList(inp.split(" ")));
      String cmd = args.get(0);
      args.remove(0);

      if (cmd.equals(Command.quit.name()) || cmd.equals(Command.exit.name())) {
        break;
      }
      if (events.containsKey(cmd)) {
        try {
          events.get(cmd).OnCommand(args);
        } catch (Exception exception) {
          LogService.error(String.format("%s\n", exception.getMessage()));
        }
      } else {
        LogService.error(String.format("Command %s not found", cmd));
      }
    }
  }

  public void onCommand(@NotNull Command command, @NotNull CommandHandler handler) {
    events.put(command.name(), handler);
  }

  public interface CommandHandler {
    void OnCommand(List<String> args) throws Exception;
  }
}
