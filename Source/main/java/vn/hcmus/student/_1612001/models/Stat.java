package vn.hcmus.student._1612001.models;

import java.util.Date;

public class Stat {
  private final long timestamp;
  private final String word;

  public Stat(long t, String word) {
    this.timestamp = t;
    this.word = word;
  }

  public Stat(String word) {
    this.timestamp = new Date().getTime();
    this.word = word;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public String getWord() {
    return word;
  }

  @Override
  public String toString() {
    return timestamp + "," + word + "\n";
  }
}
