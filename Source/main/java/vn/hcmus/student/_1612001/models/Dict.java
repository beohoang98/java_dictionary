package vn.hcmus.student._1612001.models;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.sun.istack.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "dictionary")
@XmlAccessorType(XmlAccessType.NONE)
public class Dict {
  private final Multimap<String, DictRecord> recordMap =
      Multimaps.synchronizedMultimap(HashMultimap.create());

  @XmlElement(name = "record")
  private ArrayList<DictRecord> records;

  public static @NotNull Dict load(@NotNull File file) throws JAXBException, IOException {
    FileInputStream inputStream = new FileInputStream(file);
    return load(inputStream);
  }

  public static @NotNull Dict load(@NotNull InputStream stream) throws JAXBException {
    JAXBContext jaxbContext = JAXBContext.newInstance(Dict.class);
    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    Dict dict = (Dict) unmarshaller.unmarshal(stream);

    dict.records
        .parallelStream()
        .forEach(
            (record) -> {
              dict.recordMap.put(record.getWord(), record);
            });
    return dict;
  }

  private ArrayList<DictRecord> getRecordList() {
    return new ArrayList<>(recordMap.values());
  }

  public boolean add(@NotNull String word, @NotNull String meaning) {
    DictRecord record = new DictRecord();
    record.setWord(word);
    record.setMeaning(meaning);
    return recordMap.put(word, record) && records.add(record);
  }

  public boolean remove(@NotNull DictRecord record) {
    return recordMap.remove(record.getWord(), record) && records.remove(record);
  }

  public void save(@NotNull OutputStream outputStream) throws JAXBException {
    JAXBContext context = JAXBContext.newInstance(Dict.class);
    Marshaller marshaller = context.createMarshaller();
    marshaller.marshal(this, outputStream);
  }

  public @NotNull ArrayList<DictRecord> getRecords(@NotNull String word) {
    return new ArrayList<>(recordMap.get(word));
  }
  public @NotNull ArrayList<String> getMeaning(@NotNull String word) {
    if (recordMap.containsKey(word)) {
      Set<DictRecord> records = (Set<DictRecord>) recordMap.get(word);
      return (ArrayList<String>)
          records.stream().map(DictRecord::getMeaning).collect(Collectors.toList());
    }
    return new ArrayList<>();
  }
}
