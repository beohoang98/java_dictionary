package vn.hcmus.student._1612001.services;

import com.github.tomaslanger.chalk.Chalk;

public class LogService {
  public static void log(String text) {
    System.out.print(Chalk.on(text));
  }

  public static void error(String text) {
    System.out.print(Chalk.on(text).red());
  }

  public static void info(String text) {
    System.out.print(Chalk.on(text).cyan());
  }
}
