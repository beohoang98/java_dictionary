package vn.hcmus.student._1612001.services;

import com.sun.istack.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBException;

import vn.hcmus.student._1612001.models.Dict;
import vn.hcmus.student._1612001.models.DictRecord;
import vn.hcmus.student._1612001.models.Stat;

public class TranslateService {
  public static final String vietDictResources = "Viet_Anh.xml";
  public static final String engDictResources = "Anh_Viet.xml";
  public static final String statFile = "stat.csv";

  private final Map<Languages, Dict> dictionaries = new HashMap<>();
  private Languages fromLanguage = Languages.en;
  private Map<Long, String> wordByTime = Collections.synchronizedMap(new HashMap<>());
  private FileOutputStream statOutStream;

  public TranslateService() throws IOException, JAXBException {
    InputStream vietDictStream = getClass().getClassLoader().getResourceAsStream(vietDictResources);
    InputStream engDictStream = getClass().getClassLoader().getResourceAsStream(engDictResources);

    if (vietDictStream != null && engDictStream != null) {
      loadDict(vietDictStream, Languages.vi);
      vietDictStream.close();

      loadDict(engDictStream, Languages.en);
      engDictStream.close();

//      saveDict();
      loadStat();
    }
  }

  public TranslateService(File viDict, File enDict)
      throws JAXBException, IOException {
    FileInputStream viDictStream = new FileInputStream(viDict);
    loadDict(viDictStream, Languages.vi);
    viDictStream.close();

    FileInputStream enDictStream = new FileInputStream(enDict);
    loadDict(enDictStream, Languages.en);
    enDictStream.close();

    loadStat();
  }

  void loadDict(InputStream inputStream, Languages lang) throws JAXBException {
    dictionaries.put(lang, Dict.load(inputStream));
  }

  void loadStat() throws IOException {
    File _statFile = new File(statFile);
    _statFile.createNewFile();

    Scanner scanner = new Scanner(_statFile);
    scanner.useDelimiter("[, ]");
    while (scanner.hasNextLine() && scanner.hasNextLong()) {
      Long t = scanner.nextLong();
      scanner.skip(",");
      String word = scanner.nextLine();
      wordByTime.put(t, word);
    }
    scanner.close();

    statOutStream = new FileOutputStream(_statFile, true);
  }

  public void saveDict() throws IOException, JAXBException {
    File vietDictOut = new File(vietDictResources);
    File engDictOut = new File(engDictResources);
    vietDictOut.createNewFile();
    engDictOut.createNewFile();

    dictionaries.get(Languages.vi).save(new FileOutputStream(vietDictOut, false));
    dictionaries.get(Languages.en).save(new FileOutputStream(engDictOut, false));
  }

  public Languages getFromLanguage() {
    return fromLanguage;
  }

  public void setFromLanguage(Languages lang) {
    this.fromLanguage = lang;
  }

  public List<String> getMeanings(String word) {
    collectStat(word);
    return dictionaries.get(fromLanguage).getMeaning(word);
  }
  public List<DictRecord> getRecords(String word) {
    return dictionaries.get(fromLanguage).getRecords(word);
  }
  public boolean addWord(@NotNull String word, @NotNull String meaning) {
    return dictionaries.get(fromLanguage).add(word, meaning);
  }
  public boolean removeWord(@NotNull DictRecord record) {
    return dictionaries.get(fromLanguage).remove(record);
  }

  void collectStat(String word) {
    try {
      Stat stat = new Stat(word);
      wordByTime.put(stat.getTimestamp(), word);
      statOutStream.write(stat.toString().getBytes());
      statOutStream.flush();
    } catch (IOException ioException) {
      System.out.println("cannot write stat");
    }
  }

  public void printStat() {
    wordByTime.entrySet()
        .parallelStream()
        .collect(Collectors.groupingBy(GroupByWord::group, Collectors.counting()))
        .entrySet()
        .stream()
        .sorted((e1, e2) -> (int)(e2.getValue() - e1.getValue()))
        .limit(10)
        .forEach((entry) -> {
          System.out.printf("\t%5s: %3d\n", entry.getKey(), entry.getValue());
        });
  }

  public void printStat(@NotNull Date from, @NotNull Date to) {
    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
    System.out.printf("%s ->%s\n", format.format(from), format.format(to));
    wordByTime.entrySet()
        .parallelStream()
        .filter(entry -> {
          Long t = entry.getKey();
          return t >= from.getTime() && t <= to.getTime();
        })
        .collect(Collectors.groupingBy(GroupByWord::group, Collectors.counting()))
        .entrySet()
        .stream()
        .sorted((e1, e2) -> (int)(e2.getValue() - e1.getValue()))
        .limit(10)
        .forEach((entry) -> {
          System.out.printf("\t%5s: %3d\n", entry.getKey(), entry.getValue());
        });
  }

  public static class GroupByWord {
    public static String group(Map.Entry<Long, String> entry) {
      return entry.getValue();
    }
  }

  public enum Languages {
    vi,
    en,
  }
}
