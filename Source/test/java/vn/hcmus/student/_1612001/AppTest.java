package vn.hcmus.student._1612001;

import org.junit.AfterClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;

import vn.hcmus.student._1612001.models.Dict;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AppTest {
  static InputStream oldInput = System.in;

  @AfterClass
  public static void rollback() {
    System.setIn(oldInput);
  }

  @Test
  public void A_testAppHasAGreeting() {
    App classUnderTest = new App();
    assertNotNull("app should have a greeting", classUnderTest);
  }

  @Test
  public void B_loadDataNormally() throws Exception {
    App.loadData();
    assertNotNull(App.translateService);
  }

  @Test
  public void C_loadResourcesNormally() throws Exception {
    URL url = getClass().getClassLoader().getResource("Viet_Anh.xml");
    assertNotNull(url);
    File file = new File(url.getFile());
    Dict dict = Dict.load(file);
    assertNotNull(dict);
  }

  @Test
  public void D_translateNormally() throws Exception {
    App.loadData();
    String hello = "hello";
    assertNotNull(App.translateService.getMeanings(hello));
    assertEquals(0, App.translateService.getMeanings("asdasdasd").size());
  }

  @Test
  public void E_shouldRunAndQuit() throws Exception {
    App.prompt.setScannerIn(new ByteArrayInputStream("help\nsw en\nt hello\nexit\n".getBytes()));
    App.main(new String[] {});
  }
}
